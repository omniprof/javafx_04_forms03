package com.kenfogel.javafx_04_forms03;

import com.kenfogel.javafx_04_forms03.controller.LoginFXMLController;
import com.kenfogel.javafx_04_forms03.data.UserBean;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Example of a form created in FXML and with a controller - In this approach
 * there is no need for a Presentation class as the FXML/Controller take over
 * this role.
 *
 * @author Ken
 */
public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);
    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {

        try {
            // Create a loader object for the FXML file
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Login.fxml"));

            // Use the loader to retrieve the root container. Using the container
            // superclass Parent means any container is compatible
            Parent root = loader.load();

            // If objects that can only be craeted in the FXML controller must be
            // retrieved or if messages must sent to the controller we need the
            // following code
            // Retrieve a reference to the FXML controller so that we may
            // communicate with it. In this case we will use it to get the UserBean
            LoginFXMLController controller = loader.getController();

            // Retrieve the UserBean so we can use it in other objects
            UserBean userBean = controller.getUserBean();

            Scene scene = new Scene(root);

            primaryStage.setTitle("FXML Form 03");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException | IllegalStateException ex) {
            LOG.error("Unable to load fxml", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Program Start Error");
        dialog.setHeaderText("ERROR");
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
