package com.kenfogel.javafx_04_forms03.controller;

import com.kenfogel.javafx_04_forms03.data.UserBean;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class LoginFXMLController {

    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Text actiontarget;

    private final UserBean userBean;

    /**
     * Constructor that creates the UserBean
     */
    public LoginFXMLController() {
        userBean = new UserBean();
    }

    /**
     * As the UserBean must be created in the controller, this method allows us
     * to retrieve it should we need it elsewhere
     *
     * @return
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * Event handler referred to in the FXML
     *
     * @param event
     */
    @FXML
    protected void handleSubmitButtonAction(
            ActionEvent event) {
        if (passwordField.getText().equals("Go")) {
            actiontarget.setText("Signed in as "
                    + usernameField.getText() + "!");
        } else {
            actiontarget.setText(
                    "Password not valid");
        }
    }

    @FXML
    private void initialize() {
        Bindings.bindBidirectional(usernameField.textProperty(), userBean.userNameProperty());
        Bindings.bindBidirectional(passwordField.textProperty(), userBean.userPasswordProperty());

    }

}
